//
//  AppDelegate.h
//  BanDoVN
//
//  Created by meo mun on 6/10/16.
//  Copyright © 2016 meo mun. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kAdmobBannerID                                                  @"ca-app-pub-3475889095842712/7925918182"
#define kPopUpID                                                        @"ca-app-pub-3475889095842712/1879384586"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

