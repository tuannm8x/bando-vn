//
//  CategoryViewController.h
//  BanDoVN
//
//  Created by meo mun on 6/11/16.
//  Copyright © 2016 meo mun. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CategoryViewControllerDelegate;

@interface CategoryViewController : UIViewController

@property (nonatomic, weak) id <CategoryViewControllerDelegate> delegate;

@end

@protocol CategoryViewControllerDelegate <NSObject>

@optional

- (void)categoryViewControllerDidCancel:(CategoryViewController *)controller;

- (void)categoryViewController:(CategoryViewController *)controller
                       didSelectCategory:(NSString *)category;

@end