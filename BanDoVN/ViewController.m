//
//  ViewController.m
//  BanDoVN
//
//  Created by meo mun on 6/10/16.
//  Copyright © 2016 meo mun. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "CategoryViewController.h"
#import "WYStoryboardPopoverSegue.h"
#import "AppDelegate.h"

@import GoogleMobileAds;

@interface ViewController () <UIWebViewDelegate, CLLocationManagerDelegate, CategoryViewControllerDelegate, WYPopoverControllerDelegate,
GADBannerViewDelegate, UIAlertViewDelegate>
{
    CLLocationManager *locationManager;
    WYPopoverController *categoryPopoverController;
    CLLocation *location;
}

@property (weak, nonatomic) IBOutlet UIWebView *webView1;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnLocation;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;
@property(nonatomic, strong) GADInterstitial *interstitial;

@end

@implementation ViewController

#pragma mark - Webview

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"URL: %@", request.URL.absoluteString);
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"Finish load");
    
    //hide loader
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error
{
    NSLog(@"Load fail");
    //hide loader
}

- (void) loadUrl: (NSString *) urlStr
{
    //load web
    NSString *urlString = urlStr;
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_webView1 loadRequest:urlRequest];
    
    //show loader

}

#pragma mark - Admob

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView
{
    bannerView.hidden = NO;
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"adView:didFailToReceiveAdWithError: %@", error.localizedDescription);
}

- (void) showBannerAds
{
    //admob
    _bannerView.adUnitID = kAdmobBannerID;
    _bannerView.adSize = GADAdSizeFromCGSize(CGSizeMake(320, 50));
    _bannerView.rootViewController = self;
    _bannerView.delegate = self;
    
    GADRequest *request = [GADRequest request];
    request.testDevices = @[kGADSimulatorID,
                            @"da2dfb3be791d50e6b7f04e7eda7cec2",
                            @"3e1bfb389b5fa1b15a4d534959f5ac1b",
                            @"d84aa26b19c7c1f86cfb42f802a20a8a",  // Meoden8x's iPhone
                            @"ab915b473cbe8485549f02896862d47e",
                            @"f54aae7063f4e81b743db205701a426f"
                            ];
    [_bannerView loadRequest:request];
}

- (void) loadPopUpAds
{
    self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:kPopUpID];
    
    GADRequest *request = [GADRequest request];
    // Requests test ads on test devices.
    request.testDevices = @[kGADSimulatorID,
                            @"da2dfb3be791d50e6b7f04e7eda7cec2",
                            @"3e1bfb389b5fa1b15a4d534959f5ac1b",
                            @"d84aa26b19c7c1f86cfb42f802a20a8a",  // Meoden8x's iPhone
                            @"ab915b473cbe8485549f02896862d47e",
                            @"f54aae7063f4e81b743db205701a426f"
                            ];
    [self.interstitial loadRequest:request];
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //webview
    [self loadUrl:@"http://www.google.com/maps/"];
    _webView1.delegate = self;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];

    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    //admob
    [self showBannerAds];
    [self loadPopUpAds];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UINavigationController *navigationController = segue.destinationViewController;
    CategoryViewController* cateViewController = [[navigationController viewControllers] objectAtIndex:0];
    cateViewController.preferredContentSize = CGSizeMake(260, 380);
    cateViewController.delegate = self;
    
    WYStoryboardPopoverSegue* popoverSegue = (WYStoryboardPopoverSegue*)segue;
    
    categoryPopoverController = [popoverSegue popoverControllerWithSender:sender
                                                      permittedArrowDirections:WYPopoverArrowDirectionAny
                                                                      animated:YES];
    
    categoryPopoverController.popoverLayoutMargins = UIEdgeInsetsMake(4, 4, 4, 4);
    
    categoryPopoverController.delegate = self;
}

#pragma mark - Category

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)aPopoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)aPopoverController
{
    if (aPopoverController == categoryPopoverController)
    {
        UINavigationController *navigationController = (UINavigationController *)aPopoverController.contentViewController;
        
        CategoryViewController* cateViewController = [[navigationController viewControllers] objectAtIndex:0];
        cateViewController.delegate = nil;
        
        cateViewController.delegate = nil;
        categoryPopoverController = nil;
    }

}

- (void)categoryViewControllerDidCancel:(CategoryViewController *)controller
{
    controller.delegate = nil;
    [categoryPopoverController dismissPopoverAnimated:YES];
    categoryPopoverController.delegate = nil;
    categoryPopoverController = nil;
}

- (void)categoryViewController:(CategoryViewController *)controller didSelectCategory:(NSString *)category
{
    controller.delegate = nil;
    [categoryPopoverController dismissPopoverAnimated:YES];
    categoryPopoverController.delegate = nil;
    categoryPopoverController = nil;
    
    NSLog(@"Category: %@", category);
    
    if (category!= nil && category.length > 0)
    {
        NSString *urlString = [NSString stringWithFormat:@"https://www.google.com/maps/search/%@/@%f,%f,14z", category, location.coordinate.latitude, location.coordinate.longitude];
        [self loadUrl:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:@"Bạn vui lòng cho phép ứng dụng truy cập thông tin vị trí của bạn bằng cách vào Settings > Privacy > Location Services > Bản đồ VN, chọn While Using the App"
                                              preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    //popup
    if ([self.interstitial isReady])
    {
        [self.interstitial presentFromRootViewController:self];
    }
}

#pragma mark - Location

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    location = newLocation;
}


@end
