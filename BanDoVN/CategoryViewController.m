//
//  CategoryViewController.m
//  BanDoVN
//
//  Created by meo mun on 6/11/16.
//  Copyright © 2016 meo mun. All rights reserved.
//

#import "CategoryViewController.h"

@interface CategoryViewController () <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *arrNames;
    NSArray *arrImageNames;
}

@end

@implementation CategoryViewController

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.imageView.image = [[UIImage imageNamed:[arrImageNames objectAtIndex:indexPath.row]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    cell.imageView.tintColor = [UIColor blackColor];
    cell.textLabel.text = [arrNames objectAtIndex:indexPath.row];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_delegate categoryViewController:self didSelectCategory:[arrNames objectAtIndex:indexPath.row]];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrNames = @[@"Trạm ATM", @"Bệnh viện", @"Quán cafe", @"Công viên", @"Khách sạn", @"Mua sắm", @"Xe buýt", @"Trạm xăng", @"Trường học", @"Siêu thị", @"Rạp chiếu phim", @"Nhà hàng"];
    arrImageNames = @[ @"atm_ico.png",
                       @"hospital_ico.png",
                       @"cafe_ico.png",
                       @"park_ico.png",
                       @"hotel_ico.png",
                       @"shop_ico.png",
                       @"bus_ico.png",
                       @"gas_ico.png",
                       @"school_ico.png",
                       @"supermarket_ico.png",
                       @"cinema_ico.png",
                       @"food_ico.png"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
